# Use Case Interactor

[![Build Status](https://github.com/swaggest/usecase/workflows/test/badge.svg)](https://github.com/swaggest/usecase/actions?query=branch%3Amaster+workflow%3Atest)
[![Coverage Status](https://codecov.io/gh/swaggest/usecase/branch/master/graph/badge.svg)](https://codecov.io/gh/swaggest/usecase)
[![GoDevDoc](https://img.shields.io/badge/dev-doc-00ADD8?logo=go)](https://pkg.go.dev/github.com/swaggest/usecase)
![Code lines](https://sloc.xyz/github/swaggest/usecase/?category=code)
![Comments](https://sloc.xyz/github/swaggest/usecase/?category=comments)

This module defines generalized contract of *Use Case Interactor* to enable 
[The Clean Architecture](https://blog.cleancoder.com/uncle-bob/2012/08/13/the-clean-architecture.html) 
in Go application.

![Clean Architecture](https://blog.cleancoder.com/uncle-bob/images/2012-08-13-the-clean-architecture/CleanArchitecture.jpg)

